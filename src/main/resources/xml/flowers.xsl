<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
      <body style="font-family: Arial; font-size: 12pt; background-color: #EEE">
        <div style="background-color: yellow; color: black;">
          <h2>FLOWERS</h2>
        </div>
        <table border="3">
          <tr bgcolor="#2E9AFE">
            <th>Flower ID</th>
            <th>Name</th>
            <th>Soil</th>
            <th>Origin country</th>
            <th>Stem color</th>
            <td>Leaf color</td>
            <td>Average flower size</td>
            <th>Temperature</th>
            <th>Lighting</th>
            <th>Watering</th>
            <th>Multiplying</th>
          </tr>
          <xsl:for-each select="flowers/flower">
            <tr>
              <td>
                <xsl:value-of select="@flowerID"/>
              </td>
              <td>
                <xsl:value-of select="name"/>
              </td>
              <td>
                <xsl:value-of select="soil"/>
              </td>
              <td>
                <xsl:value-of select="origin"/>
              </td>
              <td>
                <xsl:value-of select="visualParameters/stemColor"/>
              </td>
              <td>
                <xsl:value-of select="visualParameters/leafColor"/>
              </td>
              <td>
                <xsl:value-of select="visualParameters/averagePlantSize"/>
                <xsl:text> cm</xsl:text>
              </td>
              <td>
                <xsl:value-of select="growingTips/temperature"/>
                <xsl:text> C</xsl:text>
              </td>
              <td>
                <xsl:value-of select="growingTips/lighting"/>
              </td>
              <td>
                <xsl:value-of select="growingTips/watering"/>
                <xsl:text> ml</xsl:text>
              </td>
              <td>
                <xsl:value-of select="multiplying"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>