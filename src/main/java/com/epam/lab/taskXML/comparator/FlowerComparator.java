package com.epam.lab.taskXML.comparator;

import com.epam.lab.taskXML.model.Flower;

import java.util.Comparator;

public class FlowerComparator implements Comparator<Flower> {

    @Override
    public int compare(Flower o1, Flower o2) {
        return o1.getFlowerID() - o2.getFlowerID();
    }
}
