package com.epam.lab.taskXML.parser.stax;

import com.epam.lab.taskXML.model.*;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.epam.lab.taskXML.Constants.*;

public class StaxParser {
    private XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
    private Flower flower;
    private List<Flower> flowers = new ArrayList<>();
    private VisualParameters visualParameters;
    private GrowingTips growingTips;
    private Map<String, Runnable> map = new HashMap<>();

    public List<Flower> parse(File xml) {

        try {
            XMLEventReader xmlEventReader = xmlInputFactory
                    .createXMLEventReader(new FileInputStream(xml));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    xmlEvent = xmlEventReader.nextEvent();
                    final String data = xmlEvent.asCharacters().getData();
                    map.put(FLOWER_TAG, () -> {
                        flower = new Flower();
                        Attribute flowerId = startElement.getAttributeByName(new QName(FLOWER_ID_ATTRIBUTE));
                        if (flowerId != null) {
                            flower.setFlowerID(Integer.parseInt(flowerId.getValue()));
                        }
                    });
                    map.put(NAME_TAG, () -> flower.setName(data));
                    map.put(ORIGIN_TAG, () -> flower.setOrigin(data));
                    map.put(SOIL_TAG, () -> flower.setSoil(Soil.valueOf(data)));
                    map.put(MULTIPLYING_TAG, () -> flower.setMultiplying(Multiplying.valueOf(data)));
                    map.put(GROWING_TIPS_TAG, () -> growingTips = new GrowingTips());
                    map.put(TEMPERATURE_TAG, () -> growingTips.setTemperature(Integer.parseInt(data)));
                    map.put(LIGHTING_TAG, () -> growingTips.setLighting(Boolean.parseBoolean(data)));
                    map.put(WATERING_TAG, () -> growingTips.setWatering(Integer.parseInt(data)));
                    map.put(VISUAL_PARAMETERS_TAG, () -> visualParameters = new VisualParameters());
                    map.put(STEM_COLOR_TAG, () -> visualParameters.setStemColor(data));
                    map.put(LEAF_COLOR_TAG, () -> visualParameters.setLeafColor(data));
                    map.put(AVERAGE_PLANT_SIZE_TAG, () -> visualParameters.setAveragePlantSize(Double.parseDouble(data)));
                    if (null != map.get(name)) {
                        map.get(name).run();
                    }
                }
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    String name = endElement.getName().getLocalPart();
                    if (FLOWER_TAG.equals(name)) {
                        flowers.add(flower);
                        flower = null;
                    } else if (VISUAL_PARAMETERS_TAG.equals(name)) {
                        flower.setVisualParameters(visualParameters);
                        visualParameters = null;
                    } else if (GROWING_TIPS_TAG.equals(name)) {
                        flower.setGrowingTips(growingTips);
                        growingTips = null;
                    }
                }
            }
        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
        }
        return flowers;
    }
}
