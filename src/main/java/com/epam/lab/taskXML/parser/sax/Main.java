package com.epam.lab.taskXML.parser.sax;

import com.epam.lab.taskXML.model.Flower;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.List;

import static com.epam.lab.taskXML.Constants.FLOWERS_XSD_PATH;
import static com.epam.lab.taskXML.Constants.FLOWER_XML_PATH;

public class Main {
    private static final Logger LOGGER = LogManager.getLogger(Main.class);

    public static void main(String... args) {
        File xmlFile = new File(FLOWER_XML_PATH);
        File xsdFile = new File(FLOWERS_XSD_PATH);
        List<Flower> flowers = SaxParser.parse(xmlFile, xsdFile);
        LOGGER.info(flowers);
    }
}
