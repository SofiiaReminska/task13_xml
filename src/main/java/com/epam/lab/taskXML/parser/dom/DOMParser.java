package com.epam.lab.taskXML.parser.dom;

import com.epam.lab.taskXML.model.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

import static com.epam.lab.taskXML.Constants.*;

public class DOMParser {
    public static List<Flower> parse(Document document) {
        List<Flower> flowers = new ArrayList<>();
        NodeList nodeList = document.getElementsByTagName(FLOWER_TAG);
        for (int index = 0; index < nodeList.getLength(); index++) {
            Flower flower = new Flower();
            Node node = nodeList.item(index);
            Element element = (Element) node;
            flower.setFlowerID(Integer.parseInt(element.getAttribute(FLOWER_ID_ATTRIBUTE)));
            flower.setName(element.getElementsByTagName(NAME_TAG).item(0).getTextContent());
            flower.setOrigin(element.getElementsByTagName(ORIGIN_TAG).item(0).getTextContent());
            flower.setGrowingTips(parseGrowingTips(element));
            flower.setVisualParameters(parseVisualParameters(element));
            flower.setSoil(Soil.valueOf(element.getElementsByTagName(SOIL_TAG).item(0).getTextContent()));
            flower.setMultiplying(Multiplying.valueOf(element.getElementsByTagName(MULTIPLYING_TAG).item(0).getTextContent()));
            flowers.add(flower);
        }
        return flowers;
    }

    private static GrowingTips parseGrowingTips(Element element) {
        GrowingTips growingTips = new GrowingTips();
        growingTips.setTemperature(Integer.parseInt(element.getElementsByTagName(TEMPERATURE_TAG).item(0).getTextContent()));
        growingTips.setLighting(Boolean.parseBoolean(element.getElementsByTagName(LIGHTING_TAG).item(0).getTextContent()));
        growingTips.setWatering(Integer.parseInt(element.getElementsByTagName(WATERING_TAG).item(0).getTextContent()));
        return growingTips;
    }

    private static VisualParameters parseVisualParameters(Element element) {
        VisualParameters visualParameters = new VisualParameters();
        visualParameters.setStemColor(element.getElementsByTagName(STEM_COLOR_TAG).item(0).getTextContent());
        visualParameters.setLeafColor(element.getElementsByTagName(LEAF_COLOR_TAG).item(0).getTextContent());
        visualParameters.setAveragePlantSize(Double.parseDouble(element.getElementsByTagName(AVERAGE_PLANT_SIZE_TAG).item(0).getTextContent()));
        return visualParameters;
    }
}
