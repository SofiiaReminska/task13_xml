package com.epam.lab.taskXML.parser.dom;

import com.epam.lab.taskXML.model.Flower;
import com.epam.lab.taskXML.xmlValidator.XmlValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.epam.lab.taskXML.Constants.FLOWERS_XSD_PATH;
import static com.epam.lab.taskXML.Constants.FLOWER_XML_PATH;

public class Main {
    private static final Logger LOGGER = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder;
        File xmlFile = new File(FLOWER_XML_PATH);
        File xsdFile = new File(FLOWERS_XSD_PATH);

        Document document = null;
        try {
            documentBuilder = factory.newDocumentBuilder();
            document = documentBuilder.parse(xmlFile);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        if (XmlValidator.validate(document, xsdFile)) {
            List<Flower> flowerList = DOMParser.parse(document);
            LOGGER.info(flowerList);
        } else {
            LOGGER.info("XML document failed validation.");
        }
    }
}
