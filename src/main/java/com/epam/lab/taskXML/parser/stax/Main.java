package com.epam.lab.taskXML.parser.stax;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

import static com.epam.lab.taskXML.Constants.FLOWER_XML_PATH;

public class Main {
    private static final Logger LOGGER = LogManager.getLogger(Main.class);

    public static void main(String... args) {
        File xmlFile = new File(FLOWER_XML_PATH);
        StaxParser staxParser = new StaxParser();
        LOGGER.info(staxParser.parse(xmlFile));
    }
}
