package com.epam.lab.taskXML.parser.sax;

import com.epam.lab.taskXML.model.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.epam.lab.taskXML.Constants.*;

public class SaxHandler extends DefaultHandler {
    private Flower flower;
    private List<Flower> flowers = new ArrayList<>();
    private VisualParameters visualParameters;
    private GrowingTips growingTips;
    private String currentElement;
    private Map<String, Runnable> map = new HashMap<>();

    public List<Flower> getFlowerList() {
        return this.flowers;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        currentElement = qName;
        if (FLOWER_TAG.equals(currentElement)) {
            String flowerId = attributes.getValue(FLOWER_ID_ATTRIBUTE);
            flower = new Flower();
            flower.setFlowerID(Integer.parseInt(flowerId));
        } else if (VISUAL_PARAMETERS_TAG.equals(currentElement)) {
            visualParameters = new VisualParameters();
        } else if (GROWING_TIPS_TAG.equals(currentElement)) {
            growingTips = new GrowingTips();
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (FLOWER_TAG.equals(qName)) {
            flowers.add(flower);
        } else if (VISUAL_PARAMETERS_TAG.equals(qName)) {
            flower.setVisualParameters(visualParameters);
            visualParameters = null;
        } else if (GROWING_TIPS_TAG.equals(qName)) {
            flower.setGrowingTips(growingTips);
            growingTips = null;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {

        String str = new String(ch, start, length);
        map.put(NAME_TAG, () -> flower.setName(str));
        map.put(ORIGIN_TAG, () -> flower.setOrigin(str));
        map.put(SOIL_TAG, () -> flower.setSoil(Soil.valueOf(str)));
        map.put(MULTIPLYING_TAG, () -> flower.setMultiplying(Multiplying.valueOf(str)));
        map.put(TEMPERATURE_TAG, () -> growingTips.setTemperature(Integer.parseInt(str)));
        map.put(LIGHTING_TAG, () -> growingTips.setLighting(Boolean.parseBoolean(str)));
        map.put(WATERING_TAG, () -> growingTips.setWatering(Integer.parseInt(str)));
        map.put(STEM_COLOR_TAG, () -> visualParameters.setStemColor(str));
        map.put(LEAF_COLOR_TAG, () -> visualParameters.setLeafColor(str));
        map.put(AVERAGE_PLANT_SIZE_TAG, () -> visualParameters.setAveragePlantSize(Double.parseDouble(str)));
        map.get(currentElement).run();
    }
}
