package com.epam.lab.taskXML.xmlConverter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

import static com.epam.lab.taskXML.Constants.*;

public class XmlToHtmlConverter {
    private static final Logger LOGGER = LogManager.getLogger(XmlToHtmlConverter.class);

    public static void main(String[] args) {
        Source xml = new StreamSource(new File(FLOWER_XML_PATH));
        Source xslt = new StreamSource(FLOWERS_XSL_PATH);
        convertXMLToHTML(xml, xslt);
    }

    public static void convertXMLToHTML(Source xml, Source xslt) {
        StringWriter sw = new StringWriter();
        try {
            FileWriter fw = new FileWriter(FLOWERS_HTML_PATH);
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transform = tFactory.newTransformer(xslt);
            transform.transform(xml, new StreamResult(sw));
            fw.write(sw.toString());
            fw.close();
            LOGGER.info("flowers.html generated successfully.");
        } catch (IOException | TransformerFactoryConfigurationError | TransformerException e) {
            e.printStackTrace();
        }
    }
}
