package com.epam.lab.taskXML;

public class Constants {
    public static final String FLOWER_TAG = "flower";
    public static final String FLOWER_ID_ATTRIBUTE = "flowerID";
    public static final String NAME_TAG = "name";
    public static final String ORIGIN_TAG = "origin";
    public static final String SOIL_TAG = "soil";
    public static final String MULTIPLYING_TAG = "multiplying";
    public static final String TEMPERATURE_TAG = "temperature";
    public static final String LIGHTING_TAG = "lighting";
    public static final String WATERING_TAG = "watering";
    public static final String STEM_COLOR_TAG = "stemColor";
    public static final String LEAF_COLOR_TAG = "leafColor";
    public static final String AVERAGE_PLANT_SIZE_TAG = "averagePlantSize";
    public static final String GROWING_TIPS_TAG = "growingTips";
    public static final String VISUAL_PARAMETERS_TAG = "visualParameters";
    public static final String FLOWER_XML_PATH = "src\\main\\resources\\xml\\flowers.xml";
    public static final String FLOWERS_XSL_PATH = "src\\main\\resources\\xml\\flowers.xsl";
    public static final String FLOWERS_HTML_PATH = "src\\main\\resources\\xml\\flowers.html";
    public static final String FLOWERS_XSD_PATH = "src\\main\\resources\\xml\\flowers.xsd";
}
